
# Prosjekt 1 IMT2291 våren 2019 #
Prosjekt 1 IMT2291 våren 2019
Velkommen til prosjekt 1 i IMT2291 WWW-teknologi våren 2019. For å begynne å jobbe med prosjektet må en fra hver gruppe lage en fork av dette repositoriet og invitere de andre på gruppen til å delta på dette repositoriet.

Husk å velge å beholde rettigheter fra det originale repositoriet når dere oppretter forken av dette repositoriet, da får jeg automatisk tilgang til repositoriet. Sett også repositoriet til et privat repository, dere vil ikke dele koden deres med alle andre men kun de andre på gruppa.

# Prosjektdeltakere #
Kenneth Tran, Pål Syvertsen Stakvik og Sebastian Hellander

# Oppgaveteksten # 
Oppgaveteksten ligger i [Wikien til fjorårets repository](https://bitbucket.org/okolloen/imt2291-prosjekt1-2019/wiki/Home).

# Rapporten #
I dette prosjektet er kodene våre basert på labbene i faget. Mesteparten av koden brukt associative array mellom klassen og noe steder objektorientert på funksjoner. Vi har gjort løsningen vår minst mulig komplisert. 
For å gi oss et bedre perspektiv på oppgaven, utarbeidet vi oppgave-punkter for de forskjellige brukerne(adm, lærer, student), og hva slags rettigheter den enkelte har i systemet. 
Når databasen starter fra starten blir rootadm lagret med følgende brukerinfo:
Username: admroot@stud.no
Password: root

Informasjonen om admroot ligger i filen AdmRoot.php. 

For å kunne legge til mp4-fil måtte vi opprette .htacess filen i /var/www/html. Deretter legge inn følgende kode "php_value post_max_size 256M
php_value upload_max_filesize 256M, for å gjøre det mulig å laste opp større videofiler. 
Hvis det er problemer med å få tilgang til sql tables bruk kommandoen, "docker-compose down -v" og deretter "docker-compose". 
