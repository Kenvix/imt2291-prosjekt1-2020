<?php

require_once '../../vendor/autoload.php';
require_once 'classes/Users.php';
require_once 'classes/DB.php';

$loader = new \Twig\Loader\FilesystemLoader('./twig_templates');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

$data = [];
if (isset($_GET['id'])) {                           /* Er iden satt? */
  $users = new Users(DB::getDBConnection());
  $data = $users->getUser($_GET['id']);
  echo $twig->render('editUser.html', array('data'=>$data));
} else if (isset($_POST['updateUser'])) {
  $users = new Users(DB::getDBConnection());
  $data = $users->updateUser($_POST); 
  echo $twig->render('userUpdated.html',$data);
} else {
  echo $twig->render('error.html', array ("message"=>"Ingen kontaktinformasjon angitt."));
}
