<?php
/**
 * This class is for adding, listing, editing and searching contacts in
 * the contacts database.
 */
class Playlists {
  private $dsn = 'mysql:dbname=contact_registry;host=127.0.0.1';
  private $user = 'root';
  private $password = '';
  private $db = null;

  /**
   * Connect to the database when object is created.
   */
  public function __construct($db) {
    $this->db = $db;
  }

  public function __destruct() {
    if ($this->db!=null) {
      unset ($this->db);
    }
  }

  public function addPlaylist($data) {
    $sql = 'INSERT INTO playlists (owner, title, description, thumbnail) VALUES (?, ?, ?, ?)';
    $sth = $this->db->prepare($sql);
    $sth->execute(array ($data['owner'], $data['title'], $data['description'], $data['thumbnail']));  // hasher passordet executer innhold i arrayen
    $tmp = [];
    if ($sth->rowCount()==1) {
      $tmp['status'] = 'OK';
      $tmp['id'] = $this->db->lastInsertId();
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Failed to insert into playlist registry';
      $tmp['errorInfo'] = $sth->errorInfo();
    }
    return $tmp;
   } // addPlaylist()
  
   public function editPlaylist($data) {
    $sql = 'UPDATE playlists
            SET title = ?, description = ?
            WHERE id = ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($data['title'], $data['description'], $data['id']));
    if ($sth->errorInfo()[0]=='00000') {
      $tmp['status'] = 'OK';
    } else {
      $tmp['status'] = 'FAIL';
    }
    return $tmp;
    //$playlists = $sth->fetchAll(PDO::FETCH_ASSOC);
    //return $playlists;
  }

  /**
   * Kalles fra editPlaylist.php
   * Funksjon for å sjekke om spillelisten inneholder
   * noen videoer, og slette forbindelsene med dem for
   * at spillelisten kan bli slettet.
   */
  public function removeVideos($data) {
    $sql = 'SELECT *
            FROM contents
            WHERE playlist = ?';
    
    $sth = $this->db->prepare($sql);
    $sth->execute(array($data['id']));
    if ($sth->rowCount()>0) {
      $tmp['id'] = $this->db->lastInsertId();
      $tmp['hasVideos'] = 'YES';
      $tmp['status'] = 'Spillelisten har en eller flere videoer';
      $sql = "DELETE FROM contents
              WHERE playlist=?";
      $sth = $this->db->prepare($sql);
      $sth->execute(array($data['id']));
      echo "<script>console.log('Erdet vidoer her?: " . $tmp['hasVideos'] . "' )</script>";
      if ($sth->errorInfo()[0]=='00000') {
        $data['status'] = 'OK';
      } else {
        $data['status'] = 'FAIL';
      }
      echo "<script>console.log('Debug Objects: " . $tmp['status'] . "' )</script>";
    } else {
      $tmp['hasVideos'] = 'NO';
    }
    return $tmp;
  }
  
  /**
   * Kalles fra editVideo.php.
   * Slette videofilen lokalt samt
   * informasjonen om den i databasen
   */
  public function deletePlaylist($data) {
    // Slett informasjonen i databasen
    $tmp['id'] = $this->db->lastInsertId();
    $sql = "DELETE FROM playlists
            WHERE id=?";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($data['id']));
    if ($sth->errorInfo()[0]=='00000') {
      $data['status'] = 'OK';
    } else {
      $data['status'] = 'FAIL';
    }
  }

  public function fetchPlaylist($playlist) {
    $sql = 'SELECT id, owner, title, description, thumbnail
            FROM playlists
            WHERE id = ?';

    $sth = $this->db->prepare($sql);
    $sth->execute(array($playlist));
    $play = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $play;
  }

  public function fetchVideos($playlist) {
    $sql = 'SELECT video
            FROM contents
            WHERE playlist=?
            ORDER BY place';

    $sth = $this->db->prepare($sql);
    $sth->execute(array($playlist));
    $videos = $sth->fetchAll(PDO::FETCH_ASSOC); // Array med referanser som samsvarer med videos id

    $vidsInfo = array();
    foreach ($videos as $video) { // Gå gjennom array
      $videoId = $video['video'];
      $vidsInfo[] = $this->recieveVideoInfo($videoId); // videoinfo hentes og bygges på en array
    }
    return $vidsInfo;
  }

  // Hent videoinfo som skal vises til brukeren
  public function recieveVideoInfo($id) {
    $sql = "SELECT *
            FROM videos
            WHERE id=?";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($id));
    $videoInfo = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $videoInfo;
}

  public function subToPlaylist($data) {
    $sql = 'INSERT INTO subscriptions (playlist, owner)
            VALUES (?, ?)';
    $sth = $this->db->prepare($sql);
    $sth->execute(array ($data['playlist'], $data['owner']));
    $tmp = [];
    if ($sth->rowCount()==1) {
      $tmp['status'] = 'OK';
      $tmp['id'] = $this->db->lastInsertId();
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Klarte ikke abonnere';
      $tmp['errorInfo'] = $sth->errorInfo();
    }
    return $tmp;
  } // subToPlaylist()

  public function unSubToPlaylist($data) {
    $sql = 'DELETE FROM subscriptions
            WHERE owner = ?
            AND playlist = ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array ($data['owner'], $data['playlist']));
  } // unSubToPlaylist()
  
  public function checkIfSubbedToPlaylist($owner, $playlist) {
    $sql = 'SELECT *
            FROM subscriptions
            WHERE owner = ?
            AND playlist = ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array ($owner, $playlist));
    $tmp = [];
    if ($sth->rowCount()==1) {
      $tmp['subbed'] = 'YES';
    } else {
      $tmp['subbed'] = 'NO';
    }
      return $tmp;
  } // subToPlaylist()

  /**
   * Return a list of all the contacts in the database.
   *
   * @return array with the element 'status' set to 'OK' on success, 'FAIL' on failure.
   *        The element 'contacts' is an array with all contacts in the database sorted on familyName, then givenName.
   *        Each element in the array contains the id, givenName, familyName,
   *        email and phone number for the contact.
   */
  public function listPlaylists() {
    $sql = 'SELECT id, owner, title, description, thumbnail
            FROM playlists
            ORDER BY title';
    $sth = $this->db->prepare($sql);
    $sth->execute(array());
    if ($sth->errorInfo()[0]=='00000') {
      $data['status'] = 'OK';
      $data['playlists'] = $sth->fetchAll(PDO::FETCH_ASSOC);
    } else {
      $data['status'] = 'FAIL';
      $data['errorMessage'] = 'Failed to retrieve playlists from playlists registry';
      $data['errorInfo'] = $sth->errorInfo();
    }
    return $data;
  } // listPlaylists()

  public function listSubbedPlaylists($owner) {
    // Hente spilleliste-referanser i subscriptions
    $sql = "SELECT playlist
            FROM subscriptions
            WHERE owner = $owner
            ORDER BY playlist";

    $sth = $this->db->prepare($sql);
    $sth->execute(array());
    if ($sth->errorInfo()[0]=='00000') {
      $data['status'] = 'OK';
      $playlists = $data['playlists'] = $sth->fetchAll(PDO::FETCH_ASSOC);
    } else {
      $data['status'] = 'FAIL';
      $data['errorMessage'] = 'Klarte ikke hente abonnerte spillelister';
      $data['errorInfo'] = $sth->errorInfo();
    }
    
    $playsInfo = array();
    foreach ($playlists as $playlist) {
      $playlistId = $playlist['playlist'];
      $playsInfo[] = $this->recievePlaylistInfo($playlistId);
    }
    return $playsInfo;

  } // listSubbedPlaylists()

  // Med playlist-referanse, hent playlist info som skal vises til brukeren
  public function recievePlaylistInfo($id) {
    $sql = "SELECT *
            FROM playlists
            WHERE id=?";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($id));
    $playlistInfo = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $playlistInfo;
  }

  public function searchPlaylists($name) {
    $sql = 'SELECT id, owner, title, description, thumbnail
            FROM playlists
            WHERE CONCAT(title) like ?
            ORDER BY title';
    $sth = $this->db->prepare($sql);
    $sth->execute(array("%$name%"));
    if ($sth->errorInfo()[0]=='00000') {
      $data['status'] = 'OK';
      $data['playlists'] = $sth->fetchAll(PDO::FETCH_ASSOC);
      $data['search'] = $name;
    } else {
      $data['status'] = 'FAIL';
      $data['errorMessage'] = 'Klarte ikke hente spillelister fra databasen';
      $data['errorInfo'] = $sth->errorInfo();
    }
    return $data;
  } // searchPlaylists()

} // class Playlists
