<?php
session_start();
class User {
  private $uid = -1;
  private $userData = [];
  private $db;
  

  /**
   * Handles login/logout through the $_POST superglobal, handles session
   * managment trough the $_SESSION and $_COOKIE superglobals.
   *
   * @param PDO $db stores a reference to this PDO connection object,
   * used for all database interaction.
   */
  public function __construct($db) {
    $this->db = $db;

    if (isset($_POST['userName'])) {
      $this->login($_POST['userName'], $_POST['password']);
    } else if (isset($_POST['logout'])) {
      unset($_SESSION['uid']);
    } else if (isset($_SESSION['uid'])) {
      $this->uid = $_SESSION['uid'];
    }
  }
  public function loggedIn() {
    return $this->uid>-1;
  }

  /**
   * Utility function used to log in a user. Sets the $_SESSION['uid'] to the id
   * of the user if a user exists with the given username/password.
   *
   * @param  String $uname the username for the user that attempts to log in
   * @param  String $pwd   the password for the user that attempts to log in
   * @return Array        element status='OK' on success, status='FAIL' on failure.
   *                      If login failed, errorMessage contains a message indicating
   *                      if it was a bad username or a bad password.
   */
  public function login($uname, $pwd) {
    $sql = 'SELECT id, userName , password, userType FROM user WHERE userName=?';
    $sth = $this->db->prepare ($sql); /*Sender spørring til databasen*/
    $sth->execute (array($uname));    /*Gjennom fører spørringer*/
    if ($row = $sth->fetch(PDO::FETCH_ASSOC)) {        /*Henter alt innhold på raden*/
      if (password_verify($pwd, $row['password'])) {    /*Sjekker passordet mot database passordet*/
        $this->userData = $row;                         
        $_SESSION['uid'] = $row['id'];                  
        $_SESSION['uName'] = $row['userName'];
        $this->uid = $row['id'];
        if($row['userType'] == 'lærer') {              /*Lagre session ut fra hvem som er innlogget*/
           $_SESSION['userType'] = $row['userType'];
        if($row['userName'] == 'admin') {
          $_SESSION['userType'] = $row['userType'];
        }
      } 
         else {
          $_SESSION['userType'] = $row['userType'];
         }
        return array('status'=>'OK');  /*Sender en array med status ok, alt funker*/
                                      /* sende til ønskede url*/ 
      } else {
        return array('status'=>'FAIL', 'errorMessage'=>'Bad password');
      }
    }
  }

}
   