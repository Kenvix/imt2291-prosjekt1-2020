<?php

class Videos {
  private $dsn = 'mysql:dbname=contact_registry;host=127.0.0.1';
  private $user = 'root';
  private $password = '';
  private $db = null;

  /**
   * Connect to the database when object is created.
   */
  public function __construct($db) {
    $this->db = $db;
  }

  public function __destruct() {
    if ($this->db!=null) {
      unset ($this->db);
    }
  }


  /**
   * Legge til video til databasen og lokalt på disk
   */
  public function addVideo($data) {
    $sql = 'INSERT INTO videos (owner, title, course, topic, mime, size, description, likes, thumbnail)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($data['owner'],
                        $data['title'],
                        $data['course'],
                        $data['topic'],
                        $data['mime'],
                        $data['size'],
                        $data['description'],
                        $data['likes'],
                        $data['thumbnail']));
    $tmp = [];
    if ($sth->rowCount()==1) {
      $id = $this->db->lastInsertId();
      if (!file_exists('uploadedFiles/'.$data['owner'])) {
        @mkdir('uploadedFiles/');
        @mkdir('uploadedFiles/'.$data['owner']);
      }
      if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], "uploadedFiles/{$data['owner']}/$id")) {
        $tmp['id'] = $this->db->lastInsertId();
        $tmp['status'] = 'OK';
      }
      else {
        $tmp['id'] = $this->db->lastInsertId();
        $tmp['skjer'] = "klarte å sette inn data i db, men ikke lagre fil på disk (uploadedFiles/{$data['owner']}/$id), tar derfor og sletter raden som ble satt i db";
        $tmp['status'] = 'halvveis';
        $sql = "DELETE FROM videos
                WHERE id=?";
        $sth = $this->db->prepare($sql);
        $sth->execute(array($tmp['id']));
      }
      return $tmp;
    } // if ($sth->rowCount()==1)
    else {
      $tmp['status'] = 'FAIL';
      $tmp['skjer'] = "Klarte ikke sette inn i database";
    }
    return $tmp;
  
  } // addVideo()

  public function fetchVideo($id) {
    $sql = 'SELECT id, owner, title, mime, size, description, likes, thumbnail
            FROM videos
            WHERE id = ?';

    $sth = $this->db->prepare($sql);
    $sth->execute(array($id));
    $videos = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $videos;
  }

  public function editVideo($data) {
    $sql = 'UPDATE videos
            SET title = ?, description = ?
            WHERE id = ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($data['title'], $data['description'], $data['id']));
    if ($sth->errorInfo()[0]=='00000') {
      $tmp['status'] = 'OK';
    } else {
      $tmp['status'] = 'FAIL';
    }
    return $tmp;
    //$videos = $sth->fetchAll(PDO::FETCH_ASSOC);
    //return $videos;
  }

  /**
   * Kalles fra editVideo.php
   * Funksjon for å sjekke om det er noen – og slette – kommentarer på videoen
   * brukeren prøver å slette. Kommentarer som refererer til videoen
   * må slettes før videoen kan slettes
   */
  public function deleteComments($data) {
    $sql = 'SELECT *
            FROM comments
            WHERE video = ?';
    
    $sth = $this->db->prepare($sql);
    $sth->execute(array($data['id']));
    if ($sth->rowCount()>0) {
      $tmp['id'] = $this->db->lastInsertId();
      $tmp['comments'] = 'YES';
      $tmp['status'] = 'Det er kommentar(er) på videoen';
      $sql = "DELETE FROM comments
              WHERE video=?";
      $sth = $this->db->prepare($sql);
      $sth->execute(array($data['id']));
      if ($sth->errorInfo()[0]=='00000') {
        $data['status'] = 'OK';
      } else {
        $data['status'] = 'FAIL';
      }
    } else {
      $tmp['comments'] = 'NO';
    }
    return $tmp;
  }

  /**
   * Kalles fra editVideo.php
   * Funksjon for å sjekke om det er noe – og slette – informasjon om videoen
   * brukeren prøver å slette. Informasjon om hvilke spillelister en video hører
   * til må slettes før videoen kan slettes
   */
  public function removeFromPlaylists($data) {
    $sql = 'SELECT *
            FROM contents
            WHERE video = ?';
    
    $sth = $this->db->prepare($sql);
    $sth->execute(array($data['id']));
    if ($sth->rowCount()>0) {
      $tmp['id'] = $this->db->lastInsertId();
      $tmp['inPlaylists'] = 'YES';
      $tmp['status'] = 'Videoen er i spilleliste(r)';
      $sql = "DELETE FROM contents
              WHERE video=?";
      $sth = $this->db->prepare($sql);
      $sth->execute(array($data['id']));
      if ($sth->errorInfo()[0]=='00000') {
        $data['status'] = 'OK';
      } else {
        $data['status'] = 'FAIL';
      }
    } else {
      $tmp['inPlaylists'] = 'NO';
    }
    return $tmp;
  }

  
  /**
   * Kalles fra editVideo.php.
   * Slette videofilen lokalt samt
   * informasjonen om den i databasen
   */
  public function deleteVideo($data) {
    if (file_exists('uploadedFiles/'.$data['owner'])) {
      $data['status'] = "Mappen finnes";
      if (file_exists('uploadedFiles/'.$data['owner']. '/' .$data['id'])) {
        $data['status'] = "Filen finnes";
        $file_pointer = 'uploadedFiles/'.$data['owner']. '/' .$data['id'];
        // Slett filen som ligger i denne path'en
        if (!unlink($file_pointer)) {
          $data['slettet'] = "FAIL";
        }  
        else {  
          $data['slettet'] = "OK";
        }  
      }
    }
    /* Hvis filen ikke lenger eksisterer, vi ønsker kun å slette informasjonen
    på databasen om filen lokalt er slettet*/
    
    if (!file_exists('uploadedFiles/'.$data['owner']. '/' .$data['id'])) {
      // Slett informasjonen i databasen
      $tmp['id'] = $this->db->lastInsertId();
      $sql = "DELETE FROM videos
              WHERE id=?";
      $sth = $this->db->prepare($sql);
      $sth->execute(array($data['id']));
      if ($sth->errorInfo()[0]=='00000') {
        $data['status'] = 'OK';
      } else {
        $data['status'] = 'FAIL';
      }
    }
   return $data;
  }
  
  public function fetchPlaylistsToDropd() {
    $sql = 'SELECT id, title
            FROM playlists';

    $sth = $this->db->prepare($sql);
    $sth->execute(array());
    $playlistsDropd = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $playlistsDropd;
  }

  public function fetchPlaylistsContainingVideo($id) {
    // Hent spilleliste referanser
    $sql = 'SELECT playlist
            FROM contents
            WHERE video=?';

    $sth = $this->db->prepare($sql);
    $sth->execute(array($id));
    $playlists = $sth->fetchAll(PDO::FETCH_ASSOC);
    
    $playlistsInfo = array();
    foreach ($playlists as $playlist) {
      $playlistId = $playlist['playlist'];
      $playlistsInfo[] = $this->recievePlaylistsInfo($playlistId);
    }
    return $playlistsInfo;
  }

  public function recievePlaylistsInfo($id) {
    $sql = "SELECT *
            FROM playlists
            WHERE id=?";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($id));
    $playlistsInfo = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $playlistsInfo;
  }

  public function addVideoToPlaylist($data) {
    $sql = 'INSERT INTO contents (place, playlist, video)
            VALUES (?, ?, ?)';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($data['place'], $data['playlist'], $data['video']));
    $tmp = [];
    if ($sth->rowCount()==1) {
      $tmp['id'] = $this->db->lastInsertId();
      $tmp['status'] = 'OK';
    } else {
      $tmp['status'] = 'FAIL';
    }
    return $tmp;
  } // addVideoToPlaylist()


  public function listVideos() {
    $sql = 'SELECT id, owner, title, course, topic, mime, size, description, likes, thumbnail
            FROM videos
            ORDER BY title';
    $sth = $this->db->prepare($sql);
    $sth->execute(array());
    if ($sth->errorInfo()[0]=='00000') {
      $data['status'] = 'OK';
      $data['videos'] = $sth->fetchAll(PDO::FETCH_ASSOC);
    } else {
      $data['status'] = 'FAIL';
      $data['errorMessage'] = 'Klarte ikke hente videoer';
      $data['errorInfo'] = $sth->errorInfo();
    }
    return $data;
  } // listVideos()


  public function searchVideos($name) {
    $sql = 'SELECT id, owner, title, course, topic, mime, size, description, likes, thumbnail
            FROM videos
            WHERE CONCAT(title, " ", course, " ", topic) like ?
            ORDER BY title';
    $sth = $this->db->prepare($sql);
    $sth->execute(array("%$name%"));
    if ($sth->errorInfo()[0]=='00000') {
      $data['status'] = 'OK';
      $data['videos'] = $sth->fetchAll(PDO::FETCH_ASSOC);
      $data['search'] = $name;
    } else {
      $data['status'] = 'FAIL';
      $data['errorMessage'] = 'Klarte ikke hente videoer fra databasen';
      $data['errorInfo'] = $sth->errorInfo();
    }
    return $data;
  } // searchVideos()

 public function getComment() {
    //Henter kommentar(er) og brukerID(owner)
    $sql = 'SELECT user.userName, comments.content
            FROM comments 
            INNER JOIN user
            ON comments.owner = user.id
            WHERE video=?';
    
    $sth = $this->db->prepare($sql);
    $sth->execute(array($_GET['id']));
    $displayComments = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $displayComments;
  }
  
  /*public function compareNames(){
    $sql = "SELECT user.id, user.userName
            FROM user
            INNER JOIN comments
            ON user.id = comments.owner
            ORDER BY user.userName";
    
    $sth = $this->db->prepare($sql);
    $sth->execute(array($_GET['id']));
    $compareName = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $compareName;
    }*/

} // class Videos





























//echo $twig->render('listVideos.html', array('files' => $result));
