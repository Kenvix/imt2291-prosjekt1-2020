<?php
class AdmRoot {
  private $dsn = 'mysql:dbname=contact_registry;host=127.0.0.1';
  private $user = 'root';
  private $password = '';
  
  private $db;
  private $uid = -1;
  private $userData = [];
 
  public function __construct($db) {
    $this->db = $db;
    $data['userName'] = 'admroot@stud.no';
    $data['password'] = 'root';
    $data['userType'] = 'adm';
    $this->createRootAdm($data);
  }

  public function createRootAdm($data) {
    $userName = $data['userName'];
    $sql = "select userName from user where userName='$userName'";        
    $sth = $this->db->prepare($sql);                                    
    $sth->execute(array($userName));                                      
    $res=$sth->fetch(PDO::FETCH_ASSOC);
    $tmp = [];  

    if($res['userName']==$userName){
      $tmp['status'] = 'FAIL';                                         
      $tmp['errorMessage'] = 'Failed to insert into user registry';
      $tmp['errorInfo'] = $sth->errorInfo();
    }
     else {
       $sql = 'insert into user (userName, password, userType) VALUES (?, ?, ?)';
       $sth = $this->db->prepare ($sql);
       $sth->execute (array ($data['userName'], password_hash($data['password'],PASSWORD_DEFAULT), $data['userType']));  // hasher passordet executer innhold i arrayen
       $tmp = [];
       if ($sth->rowCount()==1) {
           $tmp['status'] = 'OK';
           $tmp['id'] = $this->db->lastInsertId();
          } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert into user registry';
            $tmp['errorInfo'] = $sth->errorInfo();
            }
            return $tmp;
           }
      }

}





  