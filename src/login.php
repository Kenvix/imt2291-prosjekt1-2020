<?php
session_start();
require_once '../../vendor/autoload.php';
require_once "classes/User.php";
require_once "classes/DB.php";
require_once "classes/Users.php";

$loader = new \Twig\Loader\FilesystemLoader('./twig_templates');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

$db = DB::getDBConnection();


if ($db==null) {
  echo $twig->render('error.html', array('msg' => 'Unable to connect to the database!'));
  die();  // Abort further execution of the script
}
if($_SESSION['userType'] == 'student'){    /* Hvis brukeren er lærer sender brukeren til lærersiden */
  header('Location: student.php');                  
}
if($_SESSION['userType'] == "lærer"){
  header('Location: laerer.php');
}
if($_SESSION['userType'] == "adm"){
  header('Location: adm.php');
}

 
  
