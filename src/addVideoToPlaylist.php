<?php
require_once 'setup.php';

$db = DB::getDBConnection();

if (isset($_POST['playlist-select'])) {
  $data['place'] = 0; // lagre rekkefølge i db, skulle helst økt automatisk, men det er bare én kolonne som kan være auto increment
  $data['playlist'] = $_POST['playlist-select']; // <option value="{{playlist.id}}">
  $data['video'] = $_POST['video']; // <input type="hidden" value="{{video.id}}">

  $videosInPlaylist = new Videos($db);
  $res = $videosInPlaylist->addVideoToPlaylist($data);
  $res['data'] = $data;
  
  echo $twig->render('videoAddedToPlaylist.html', array('data' => $res, 'session' => $_SESSION));
}