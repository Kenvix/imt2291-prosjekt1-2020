<?php
require_once 'setup.php';

if (!isset($_POST['title'])) {
  echo $twig->render('addPlaylistForm.html', array('session'=>$_SESSION));
} else {
  $data['owner'] = $_POST['uid'];
  $data['title'] = $_POST['title'];
  $data['description'] = $_POST['desc'];
  $data['thumbnail'] = $_POST['thumbnail'];
  
  $db = DB::getDBConnection();  
  if ($db==null) {
    // show error page and exit
  }
  $playlists = new Playlists($db);
  $res = $playlists->addPlaylist($data);           
  $res['data'] = $data;

  echo $twig->render('playlistAdded.html', $res);
}

