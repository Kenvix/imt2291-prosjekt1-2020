<?php
require_once 'setup.php';


  $db = DB::getDBConnection();  
  if (!isset($_POST['content'])) {  // Changed from "addUser" to work with mink/GoutteDriver
    echo $twig->render('video.html', array());
  } else {
    $data['owner'] = ($_POST['owner']);
    $data['video'] = ($_POST['video']);
    $data['content'] = ($_POST['content']);
    //echo "<script>console.log(Variabel " . $_POST['owner'] ."</script> )";
    //echo "<script>console.log(Variabel " . $_GET['id'] ."</script> )";
    //echo "<script>console.log(Variabel " . $_POST['content'] . "</script> )";
    $users = new Users($db);
    $res = $users->addComment($data);           
    $res['data'] = $data;
    
    echo $twig->render('video.html', $res);
     
  }