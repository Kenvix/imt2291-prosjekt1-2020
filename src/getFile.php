<?php
//nødvendig fil?
require_once 'classes/DB.php';

$db = DB::getDBConnection();

$sql = "SELECT id, owner, title, mime, size, description, likes, thumbnail
        FROM videos
        WHERE id=?";
$sth = $db->prepare($sql);
$sth->execute(array($_GET['id']));

if ($row=$sth->fetch(PDO::FETCH_ASSOC)) {
  if (file_exists("uploadedFiles/{$row['owner']}/{$row['id']}")) {
    header('Content-type: '.$row['mime']);
    header('Content-Disposition: attachment; filename='.$row['name']);
    header('Content-Length: ' . $row['size']);
    readfile ("uploadedFiles/{$row['owner']}/{$row['id']}");
    die();
  }
}
header("HTTP/1.0 404 Not Found");
