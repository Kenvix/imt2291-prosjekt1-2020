<?php
require_once "setup.php";

$playlists = new Playlists(DB::getDBConnection());
$playsInfo = $playlists->listSubbedPlaylists($_SESSION['uid']);

if ($db==null) {
  echo $twig->render('error.html', array('msg' => 'Unable to connect to the database!'));
  die();  // Abort further execution of the script
}
echo $twig->render('student.html', array('playsInfo' => $playsInfo, 'session'=> $_SESSION)); /*Sender session videre til htmlen*/

