<?php
require_once 'setup.php';

$playlists = new Playlists(DB::getDBConnection());
$res = $playlists->listPlaylists();

echo $twig->render('listPlaylists.html', $res);
