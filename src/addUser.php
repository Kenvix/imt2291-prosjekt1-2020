<?php
require_once 'setup.php';

if (!isset($_POST['userName'])) {  // Changed from "addUser" to work with mink/GoutteDriver
  echo $twig->render('addUserForm.html', array());
} else {
  $data['userName'] = ($_POST['userName']);
  $data['password'] = ($_POST['password']); 
  $data['userType'] = ($_POST['userType']); 
  
  if($data['userType'] == ''){              
     $data['userType'] = 'student';
   } else {
     // linje som varsle adm at studenten har kryssa på lærer 
   $data['userType'] = 'lærer';         // Mangler at bare ADM kan gi rettigheter til at studenten kan bli lærer. 
   }
  $db = DB::getDBConnection();  
  /*
  if ($db==null) {
    // show error page and exit
  } */
  $users = new Users($db);
  $res = $users->addUser($data);           
  $res['data'] = $data;

  echo $twig->render('userAdded.html', $res);
}

