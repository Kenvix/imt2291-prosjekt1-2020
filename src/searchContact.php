<?php

require_once '../../vendor/autoload.php';
require_once 'classes/Contacts.php';
require_once 'classes/DB.php';

$loader = new \Twig\Loader\FilesystemLoader('./twig_templates');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

$data = [];
if  (isset($_GET['search'])) {
  $contacts = new Contacts(DB::getDBConnection());
  $data = $contacts->searchContacts ($_GET['search']);
}

echo $twig->render('searchContacts.html', $data);
