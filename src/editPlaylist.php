<?php
require_once 'setup.php';

$db = DB::getDBConnection();

// Hente spilleliste
$playlist = new Playlists($db);
$playlists = $playlist->fetchPlaylist($_GET['id']);

if (isset($_POST['editPlaylist'])) {
  $data['title'] = $_POST['title'];
  $data['description'] = $_POST['descr'];
  $data['id'] = $_POST['id'];
  $playlist = new Playlists($db);
  $playlists = $playlist->editPlaylist($data);
  $res['data'] = $data;
  echo $twig->render('playlistEdited.html', array('data' => $res, 'playlists' => $playlists, 'session' => $_SESSION));
}
else if (isset($_POST['deletePlaylist'])) {
  // Hente playlistens id og eier
  $data['id'] = $_POST['id'];
  $data['owner'] = $_POST['owner'];

  // Slette evt videoforbindelser
  $hasVideo = new Playlists($db);
  $hasVideos = $hasVideo->removeVideos($data);

  // Slette spillelisten
  $playlist = new Playlists($db);
  $playlists = $playlist->deletePlaylist($data);
  $res['data'] = $playlists;
  echo $twig->render('playlistRemoved.html', array('data' => $res, 'playlists' => $playlists, 'session' => $_SESSION));
}
else {
  echo $twig->render('editPlaylist.html', array('playlists' => $playlists, 'session' => $_SESSION));
}