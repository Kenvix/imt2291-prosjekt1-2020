<?php
session_start();
require_once '../../vendor/autoload.php';
require_once "classes/User.php";
require_once "classes/Users.php";
require_once "classes/Videos.php";
require_once "classes/Playlists.php";
require_once "classes/DB.php";


$loader = new \Twig\Loader\FilesystemLoader('./twig_templates');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

$db = DB::getDBConnection();

$user = new User($db);

// if ($user->loggedIn()) {
//   echo "Innlogget som " . $_SESSION['uName'] . ". Id er " . $_SESSION['uid'] . "<br>";
// }
?>