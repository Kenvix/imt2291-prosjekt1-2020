<?php
session_start();
require_once '../../vendor/autoload.php';
require_once "classes/User.php";
require_once "classes/DB.php";
require_once "classes/Users.php";

$loader = new \Twig\Loader\FilesystemLoader('./twig_templates');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

$db = DB::getDBConnection();


if ($db==null) {
  echo $twig->render('error.html', array('msg' => 'Unable to connect to the database!'));
  die();  // Abort further execution of the script
}
  echo $twig->render('lærer.html', array('session'=> $_SESSION)); /*Sender session videre til htmlen*/

  
  
