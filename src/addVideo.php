<?php
require_once "setup.php";

if (!isset($_FILES['fileToUpload'])) {
  echo $twig->render('addVideoForm.html', array('session'=>$_SESSION));
  exit(); // Slutte å lese resten av koden i denne filen
}

if (is_uploaded_file($_FILES['fileToUpload']['tmp_name'])) {
  $data['owner'] = $_POST['uid']; // <input type="hidden" name="uid" value="{{session.uid}}">
  $data['title'] = $_POST['title']; // <input type="text" name="title">
  $data['course'] = $_POST['course']; // <input type="text" name="course">
  $data['topic'] = $_POST['topic']; // <input type="text" name="topic">
  $data['mime'] = $_FILES['fileToUpload']['type'];
  $data['size'] = $_FILES['fileToUpload']['size'];
  $data['description'] = $_POST['descr']; // <input type="text" name="descr">
  $data['likes'] = $_POST['likes']; // <input type="hidden" name="likes" value="0">
  $data['thumbnail'] = $_POST['thumbnail']; // <input type="hidden" name="thumbnail" value="1">
  /*
  if ($db==null) {
    // show error page and exit
  } */
  $videos = new Videos($db);
  $res = $videos->addVideo($data);           
  $res['data'] = $data;

  echo $twig->render('videoAdded.html', $res);

} // if (is_uploaded_file($_FILES['fileToUpload']['tmp_name']))
else {  // Some trickery is going on
  echo $twig->render('badbadbad.html', array());
}
