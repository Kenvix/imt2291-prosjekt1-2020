<?php
require_once 'setup.php';

$users = new Users(DB::getDBConnection());
$res = $users->listUsers();

echo $twig->render('listUsers.html', $res);
