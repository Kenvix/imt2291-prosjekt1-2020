<?php
require_once 'setup.php';

$data = [];
if (isset($_GET['search'])) {
  $videos = new Videos(DB::getDBConnection());
  $data = $videos->searchVideos($_GET['search']);
}

echo $twig->render('searchVideos.html', $data);
