<?php
require_once 'setup.php';

// Hente spillelisteinfo
$playlist = new Playlists(DB::getDBConnection());
$playInfo = $playlist->fetchPlaylist($_GET['id']);

// Hente videoinfo i spilleliste
$video = new Playlists(DB::getDBConnection());
$vidsInfo = $video->fetchVideos($_GET['id']);

$res = array();
if (isset($_POST['subscribe'])) {
  $data['owner'] = $_POST['uid'];
  $data['playlist'] = $_POST['playlist'];

  $playlists = new Playlists($db);
  $res = $playlists->subToPlaylist($data);
  $res['data'] = $data;
}

if (isset($_POST['unSubscribe'])) {
  $data['owner'] = $_POST['uid'];
  $data['playlist'] = $_POST['playlist'];

  $playlists = new Playlists($db);
  $res = $playlists->unSubToPlaylist($data);
  $res['data'] = $data;
}

$subscribed = new Playlists(DB::getDBConnection());
$subbed = $subscribed->checkIfSubbedToPlaylist($_SESSION['uid'], $_GET['id']);

echo $twig->render('playlist.html', array(
  'data' => $res,
  'files' => $playInfo, // Playlistinfo
  'vidsInfo' => $vidsInfo, // Videoinfo-liste
  'subbed' => $subbed,
  'session' => $_SESSION
));