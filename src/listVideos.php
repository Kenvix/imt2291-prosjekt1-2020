<?php
require_once 'setup.php';

$videos = new Videos(DB::getDBConnection());
$res = $videos->listVideos();

echo $twig->render('listVideos.html', $res);
