<?php
require_once 'setup.php';

$db = DB::getDBConnection();

$displayComments;
$compareName;
// Hente videoinfo
$video = new Videos($db);
$videos = $video->fetchVideo($_GET['id']);

// Hente spillelister til legge-video-til-spilleliste dropdown
$playlist = new Videos($db);
$playlistsDropd = $playlist->fetchPlaylistsToDropd();

// Hente spillelister videoen er med i
$playlist = new Videos($db);
$playlistsInfo = $playlist->fetchPlaylistsContainingVideo($_GET['id']);

$getcomments = new Videos($db);
$displayComments = $getcomments->getComment();


echo $twig->render('video.html', array(
  'videos' => $videos, // videoinfo
  'playlistsDropd' => $playlistsDropd, // playlistinfo-dropdown
  'playlistsInfo' => $playlistsInfo, // playlistinfo-liste
  'compareName' => $compareName,
  'displayComments' => $displayComments,
  'session' => $_SESSION
));