<?php
require_once 'setup.php';

$data = [];
if (isset($_GET['search'])) {
  $playlists = new Playlists(DB::getDBConnection());
  $data = $playlists->searchPlaylists($_GET['search']);
}

echo $twig->render('searchPlaylists.html', $data);
