<?php
require_once 'setup.php';

$db = DB::getDBConnection();

// Hente videoinfo
$video = new Videos($db);
$videos = $video->fetchVideo($_GET['id']);

if (isset($_POST['editVideo'])) {
  $data['title'] = $_POST['title'];
  $data['description'] = $_POST['descr'];
  $data['id'] = $_POST['id'];
  $video = new Videos($db);
  $videos = $video->editVideo($data);
  $res['data'] = $data;
  echo $twig->render('videoEdited.html', array('data' => $res, 'videos' => $videos, 'session' => $_SESSION));
}
else if (isset($_POST['deleteVideo'])) {
  /* før video kan slettes, må evt. informasjon
  om videoen er i spillelister og kommentarer
  på den slettes først. Slette elementer med
  foreign key constraint til denne videoen. */

  // Hente videoens id og eier
  $data['id'] = $_POST['id'];
  $data['owner'] = $_POST['owner'];
  
  // Slette evt tilstedeværelse i spillelister
  $inPlaylist = new Videos($db);
  $inPlaylists = $inPlaylist->removeFromPlaylists($data);
  
  // Slette evt kommentarer
  $comment = new Videos($db);
  $comments = $comment->deleteComments($data);

  // Slette videoinformasjonen på databasen
  // og selve videoen som ligger lokalt
  $video = new Videos($db);
  $videos = $video->deleteVideo($data);
  $res['data'] = $videos;
  echo $twig->render('videoRemoved.html', array('data' => $res, 'videos' => $videos, 'session' => $_SESSION));
}
else {
  echo $twig->render('editVideo.html', array('videos' => $videos, 'session' => $_SESSION));
}