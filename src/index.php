<?php
require_once "setup.php";
require_once "classes/AdmRoot.php";


if ($db == null) {
  echo $twig->render('error.html', array('msg' => 'Unable to connect to the database!'));
  die();  // Abort further execution of the script
}
$adm = new AdmRoot($db);

if ($user->loggedIn()) {
  echo $twig->render('index.html', array('loggedin' => 'yes'), $res); // Add other data as needed
} else {
  echo $twig->render('index.html', array());  // Add data as needed
}
